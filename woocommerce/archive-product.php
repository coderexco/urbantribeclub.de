<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
* hero slider shortcode
*/
echo do_shortcode(get_theme_mod('urban_tribe_shop_hero_slider'));

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );
    
    
    /**
    * shop product filter by categorie
    */
    echo '<div class="product-cat-wrapper">';
    $cat_args = array(
        'orderby'    => 'name',
        'order'      => 'asc',
        'hide_empty' => true,
    );

    $product_cats = get_terms( 'product_cat', $cat_args );

    foreach($product_cats as $key => $cat){
        
        echo '<div class="single-categorie-product category_title '.$cat->slug. ' ' . $class .'">';
        echo '<h2>'.$cat->name.'</h2>';        
        $args = array(
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page'        => '-1',
            'tax_query'             => array(
                array(
                    'taxonomy'      => 'product_cat',
                    'field' => 'term_id',
                    'terms'         => $cat->term_id,
                    'operator'      => 'IN' 
                )
            )
        );
        
        $products = new WP_Query($args);
        
        woocommerce_product_loop_start();
        
        if ( wc_get_loop_prop( 'total' ) ) {
            while ( $products->have_posts() ) {
                $products->the_post();
                
                /*
                * Hook: woocommerce_shop_loop.
                */
                    
                do_action( 'woocommerce_shop_loop' );
                
                wc_get_template_part( 'content', 'product' );
            }
        }
        
        woocommerce_product_loop_end();
        
        echo "</div>"; //---end single categorie----
    }
    
    echo '</div>'; //---product-cat-wrapper---

	

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	//do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
* reference shortcode
*/
echo do_shortcode(get_theme_mod('urban_tribe_shop_reference_shortcode'));

/**
* Map shortcode
*/
echo do_shortcode(get_theme_mod('urban_tribe_shop_map_shortcode'));



get_footer( 'shop' );
