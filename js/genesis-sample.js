/**
 * Genesis Sample entry point.
 *
 * @package GenesisSample\JS
 * @author  StudioPress
 * @license GPL-2.0-or-later
 */
(function (document, $) {


    $(document).ready(function () {
        
        var lastWidth = $(window).width();
        myblogroll();
        if (lastWidth >= "722" && lastWidth <= "1300") {
            myFunction();
        }
        $(window).resize(function () {

            myblogroll();
            if ($(window).width() != lastWidth) {
                if (lastWidth >= "722" && lastWidth <= "1300") {
                    $(".mobile-image").height("");
                    $(".boxed_text").height("");
                    myFunction();
                } else {
                    $(".mobile-image").css('cssText', '');
                    $(".boxed_text").css('cssText', '');

                }
                if (lastWidth >= "1048") {
                    $(".site-header").removeClass("fix");
                }
            }
            lastWidth = $(window).width();
        });
        
        
        //-----------------shop page product filter------------
          var single_items = $(".single-categorie-product");
        
        $(".woocommerce-category_title li").on("click", function() {
            var menu_item = $(this).attr('data-attr');
            $('.woocommerce-category_title li').removeClass('active');
            $(this).addClass('active');
         
            
            if (menu_item == "all") {
              single_items.show();
            } else {
              var each_element = $("." + menu_item).show();
              single_items.not(each_element).hide();
            }
        });
        
        //--------add to cart increase or decrease-----
        $(".product__increse").on('click', function () {
            var old = $(this).parent('.quantity').find('.qty').val();
            var newn = parseInt(old);
            $(this).parent('.quantity').find('.qty').val(newn + 1);
        });

        $(".product__decrese").on('click', function () {
            var old = $(this).parent('.quantity').find('.qty').val();
            var newn = parseInt(old);
            if (newn > 1) {
                $(this).parent('.quantity').find('.qty').val(newn - 1);
            }
        });
        
        //--------single product carousel-----
        $('.flex-control-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow: '<button class="PrevArrow"><i class="fa fa-long-arrow-left"></i></button>',
            nextArrow: '<button class="NextArrow"><i class="fa fa-long-arrow-right"></i></button>',
            focusOnSelect: true
        });
        
        //---additional add to cart button "text change" on product single page--
        $("#additional-add-to-cart button.button").text("JETZT KAUFEN");
        
        //---header menu "cart icon change"--
        $("#menu-headermenu-2 .cart-icon a span").html('<i class="fa fa-shopping-cart"></i>');
        
    });

    function myFunction() {
        setTimeout(function () {
            $(".boxed_text").each(function (index) {
                var fancyheader = $(this).find(".fancy-heading").outerHeight(true);
                var text = $(this).find(".module-text").outerHeight(true);
                var button = $(this).find(".module-buttons ").outerHeight(true)
                var heightbox = fancyheader + text + button + 70;
                var image = $(".mobile-image:eq(" + index + ")").height();
                var height = Math.max(heightbox, image);
                $(".mobile-image:eq(" + index + ")").css('cssText', 'height: ' + height + 'px !important');
                $(".boxed_text:eq(" + index + ")").css('cssText', 'height: ' + height + 'px !important');
            });
        }, 500);
    }

    function myblogroll() {
        var blogrollH = $(".blogroll").height();
        var blogrollW = $(".blogroll").width();
        var RblogrollH = $(".blogroll-reisen").height();
        var RblogrollW = $(".blogroll-reisen").width();
        $(".blogroll").find(".overlay").css('cssText', 'height: ' + blogrollH + 'px;width: ' + blogrollW + 'px;');
        $(".blogroll-reisen").find(".overlay").css('cssText', 'height: ' + RblogrollH + 'px;width: ' + RblogrollW + 'px;');
    }
    $(document).ready(function () {

        $(".site-header").addClass("aktiv");

        var lastWidth = $(window).width();
        if (lastWidth <= "1048") {
            $(window).scroll(function (event) {
                var scroll = $(window).scrollTop();
                $(window).bind('mousewheel', function (event) {
                    if (scroll >= "120") {
                        if (event.originalEvent.wheelDelta >= 0) {
                            $(".site-header").addClass("fix");
                        } else {
                            $(".site-header").removeClass("fix");
                        }
                    } else {
                        $(".site-header").removeClass("fix");
                    }
                });


            });
        }
    });

})(document, jQuery);

var genesisSample = (function ($) {
    'use strict';

    /**
     * Adjust site inner margin top to compensate for sticky header height.
     *
     * @since 2.6.0
     */
    var moveContentBelowFixedHeader = function () {
            var siteInnerMarginTop = 0;

            if ($('.site-header').css('position') === 'fixed') {
                siteInnerMarginTop = $('.site-header').outerHeight();
            }

            $('.site-inner').css('margin-top', siteInnerMarginTop);
        },

        /**
         * Initialize Genesis Sample.
         *
         * Internal functions to execute on full page load.
         *
         * @since 2.6.0
         */
        load = function () {
            moveContentBelowFixedHeader();

            $(window).resize(function () {
                moveContentBelowFixedHeader();
            });

            // Run after the Customizer updates.
            // 1.5s delay is to allow logo area reflow.
            if (typeof wp != "undefined" && typeof wp.customize != "undefined") {
                wp.customize.bind('change', function (setting) {
                    setTimeout(function () {
                        moveContentBelowFixedHeader();
                    }, 1500);
                });
            }
        };

    // Expose the load and ready functions.
    return {
        load: load
    };

})(jQuery);

jQuery(window).on('load', genesisSample.load);
