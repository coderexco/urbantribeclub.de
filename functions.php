<?php
/**
 * Genesis Sample.
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0-or-later
 * @link    https://www.studiopress.com/
 */

// Starts the engine.
require_once get_template_directory() . '/lib/init.php';

// Defines the child theme (do not remove).
define( 'CHILD_THEME_NAME', 'Genesis Sample' );
define( 'CHILD_THEME_URL', 'https://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.7.0' );

// Sets up the Theme.
require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );
/**
 * Sets localization (do not remove).
 *
 * @since 1.0.0
 */
function genesis_sample_localization_setup() {

	load_child_theme_textdomain( 'genesis-sample', get_stylesheet_directory() . '/languages' );

}

// Adds helper functions.
require_once get_stylesheet_directory() . '/lib/helper-functions.php';

// Adds image upload and color select to Customizer.
require_once get_stylesheet_directory() . '/lib/customize.php';

// Includes Customizer CSS.
require_once get_stylesheet_directory() . '/lib/output.php';

// Adds WooCommerce support.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php';

// Adds the required WooCommerce styles and Customizer CSS.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php';

// Adds the Genesis Connect WooCommerce notice.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php';

add_action( 'after_setup_theme', 'genesis_child_gutenberg_support' );
/**
 * Adds Gutenberg opt-in features and styling.
 *
 * @since 2.7.0
 */
function genesis_child_gutenberg_support() { // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedFunctionFound -- using same in all child themes to allow action to be unhooked.
	require_once get_stylesheet_directory() . '/lib/gutenberg/init.php';
}

add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
/**
 * Enqueues scripts and styles.
 *
 * @since 1.0.0
 */
function genesis_sample_enqueue_scripts_styles() {

	wp_enqueue_style(
		'genesis-sample-fonts',
		'//fonts.googleapis.com/css?family=Roboto:300,400,400i,600,700',
		array(),
		CHILD_THEME_VERSION
	);
    
    /**
    * slick carousel css
    */
	wp_enqueue_style('slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css', array(), CHILD_THEME_VERSION );

	wp_enqueue_style( 'dashicons' );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script(
		'genesis-sample-responsive-menu',
		get_stylesheet_directory_uri() . "/js/responsive-menus{$suffix}.js",
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);

	wp_localize_script(
		'genesis-sample-responsive-menu',
		'genesis_responsive_menu',
		genesis_sample_responsive_menu_settings()
	);

	wp_enqueue_script(
		'genesis-sample',
		get_stylesheet_directory_uri() . '/js/genesis-sample.js',
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);
    
    /**
    * slick carousel js
    */
	wp_enqueue_script('slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true);

}

/**
 * Defines responsive menu settings.
 *
 * @since 2.3.0
 */
function genesis_sample_responsive_menu_settings() {

	$settings = array(
		'mainMenu'         => '<span class="hamburger-box"><span class="hamburger-inner"></span></span>',
		'menuIconClass'    => 'hamburger hamburger--slider',
		'subMenu'          => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'      => array(
			'combine' => array(
				'.nav-primary',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

// Adds support for HTML5 markup structure.
add_theme_support(
	'html5',
	array(
		'caption',
		'comment-form',
		'comment-list',
		'gallery',
		'search-form',
	)
);

// Adds support for accessibility.
add_theme_support(
	'genesis-accessibility',
	array(
		'404-page',
		'drop-down-menu',
		'headings',
		'search-form',
		'skip-links',
	)
);

// Adds viewport meta tag for mobile browsers.
add_theme_support(
	'genesis-responsive-viewport'
);

// Adds custom logo in Customizer > Site Identity.
add_theme_support(
	'custom-logo',
	array(
		'height'      => 120,
		'width'       => 700,
		'flex-height' => true,
		'flex-width'  => true,
	)
);

// Renames primary and secondary navigation menus.
add_theme_support(
	'genesis-menus',
	array(
		'primary'   => __( 'Header left Menu', 'genesis-sample' ),
		'secondary' => __( 'Header Right Menu', 'genesis-sample' ),
	)
);

// Adds image sizes.
add_image_size( 'sidebar-featured', 75, 75, true );

// Adds support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Adds support for 3-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 3 );

// Removes header right widget area.
unregister_sidebar( 'header-right' );

// Removes secondary sidebar.
unregister_sidebar( 'sidebar-alt' );

// Removes site layouts.
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

// Removes output of primary navigation right extras.
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

add_action( 'genesis_theme_settings_metaboxes', 'genesis_sample_remove_metaboxes' );
/**
 * Removes output of unused admin settings metaboxes.
 *
 * @since 2.6.0
 *
 * @param string $_genesis_admin_settings The admin screen to remove meta boxes from.
 */
function genesis_sample_remove_metaboxes( $_genesis_admin_settings ) {

	remove_meta_box( 'genesis-theme-settings-header', $_genesis_admin_settings, 'main' );
	remove_meta_box( 'genesis-theme-settings-nav', $_genesis_admin_settings, 'main' );

}

add_filter( 'genesis_customizer_theme_settings_config', 'genesis_sample_remove_customizer_settings' );
/**
 * Removes output of header settings in the Customizer.
 *
 * @since 2.6.0
 *
 * @param array $config Original Customizer items.
 * @return array Filtered Customizer items.
 */
function genesis_sample_remove_customizer_settings( $config ) {

	unset( $config['genesis']['sections']['genesis_header'] );
	return $config;

}

// Displays custom logo.
add_action( 'genesis_site_title', 'the_custom_logo', 0 );

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
/**
 * Reduces secondary navigation menu to one level depth.
 *
 * @since 2.2.3
 *
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
 */
function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' !== $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;
	return $args;

}

add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
/**
 * Modifies size of the Gravatar in the author box.
 *
 * @since 2.2.3
 *
 * @param int $size Original icon size.
 * @return int Modified icon size.
 */
function genesis_sample_author_box_gravatar( $size ) {

	return 90;

}

add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
/**
 * Modifies size of the Gravatar in the entry comments.
 *
 * @since 2.2.3
 *
 * @param array $args Gravatar settings.
 * @return array Gravatar settings with modified size.
 */
function genesis_sample_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;
	return $args;

}

/** Adding custom Favicon */
add_filter( 'genesis_pre_load_favicon', 'custom_favicon' ); function custom_favicon( $favicon_url ) {
return 'http://kunden.yekavyan.de/urban/data-base/uploads/2018/12/favicon.png';
}

//* TN Dequeue Styles - Remove Google Fonts from Genesis Sample WordPress Theme
add_action( 'wp_print_styles', 'tn_dequeue_google_fonts_style' );
function tn_dequeue_google_fonts_style() {
      wp_dequeue_style( 'genesis-sample-fonts' );
}
// Remove the entry title (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

 // Remove Footer
 remove_action('genesis_footer', 'genesis_do_footer');
 remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
 remove_action('genesis_footer', 'genesis_footer_markup_close', 15);


//* Remove the default header
remove_action( 'genesis_header', 'genesis_do_header' );

//* Add Primary Nav in custom header
add_action( 'genesis_header', 'genesis_do_nav' );

//* Add Site Title in custom header
add_action( 'genesis_header', 'sk_do_header' );
function sk_do_header() {

	do_action( 'genesis_site_title' );
	// do_action( 'genesis_site_description' );

}

//* Add Secondary Nav in custom header
add_action( 'genesis_header', 'genesis_do_subnav' );

//* Remove Primary and Secondary Nav from below header
remove_action( 'genesis_after_header', 'genesis_do_nav' );
remove_action( 'genesis_after_header', 'genesis_do_subnav' );


function google_body_head($content) {
?><!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TJTQQF6');</script>
<!-- End Google Tag Manager -->

<?php
}
add_action('wp_head','google_body_head');

function google_body_tag($content) {
?><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJTQQF6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
}
add_action('genesis_before','google_body_tag');
// Changing Post Excerpt
function my_the_content_filter($content) {
	$postid = get_the_ID();
	$post_categories =  get_the_category( $postid);
	$feature_img = get_the_post_thumbnail_url($postid,'full'); 
	$single_url = get_the_permalink();
	$single_title = get_the_title();
	$post_date = get_the_date( 'd/m/Y' );	 
	
	if($post_categories[0]->name == "News"){
		$beschreibung = $content;
		$content = "<div class='blogroll' style='background: url(".$feature_img.") center bottom;'><a href='".$single_url."'><div class='overlay' style='width: 100%;'></div></a>";
				$content .= '<div class="full"><h3><span>'.$single_title.'</span></h3><span>'.$post_date.'</span>';
				$content .= '<div class="caption"><p>'.$beschreibung.'</p><div id="buttons-18-4-0-2" class="module module-buttons buttons-18-4-0-2 buttom_red left " data-id="b2486b4">           
        <div class="module-buttons normal solid squared"><div class="module-buttons-item  buttons-horizontal"><a href="'.$single_url.'" class="ui builder_button"><span>Weiterlesen</span></a></div></div>
    </div></div></div></div>';
	}
	
	if($post_categories[0]->name == "Test-news"){
		$beschreibung = $content;
		$content = "<div class='blogroll-news'>";
				$content .= '<div class="full grey"><div class="left"><img src="'.$feature_img.'"></div><div class="right"><h3><span>'.$single_title.'</span></h3><span>'.$post_date.'</span>';
				$content .= '<div class="caption-2"><p>'.$beschreibung.'</p><div id="buttons-18-4-0-2" class="module module-buttons buttons-18-4-0-2 buttom_red left " data-id="b2486b4">           
        <div class="module-buttons normal solid squared"><div class="module-buttons-item  buttons-horizontal"><a href="'.$single_url.'" class="ui builder_button"><span>Weiterlesen</span></a></div></div></div>
    </div></div></div></div>';
	}	
  	if (get_field('teilnehmer') || get_field('ort')) {
	

	$kursbeschreibung = get_field('kursbeschreibung');  
	$ort = get_field('ort');  
	$beschreibung = get_field('beschreibung');  
	$auszug = get_field('auszug');  	  
	$date = get_field('date');
	$schiff = get_field('segelschiff');	
	$region = get_field('region');	
	$boot = get_field('boot');	
	$mannschaft = get_field('mannschaft');	
	$soon = get_field('cooming_soon');
		
		
	  			if($post_categories[0]->name == "Kurse"){
				$content = "<div class='blogroll' style='background: url(".$feature_img.") center bottom;'><a href='".$single_url."'><div class='overlay'></div></a>";
				$content .= '<div class="full"><h3><span>'.$single_title.'</span></h3>';
				$content .= '<div class="caption"><p>'.$kursbeschreibung.'</p><div id="buttons-18-4-0-2" class="module module-buttons buttons-18-4-0-2 buttom_red left " data-id="b2486b4">           
        <div class="module-buttons normal solid squared"><div class="module-buttons-item  buttons-horizontal"><a href="'.$single_url.'" class="ui builder_button"><span>Mehr Informationen</span></a></div></div>
    </div></div></div></div>';
				} else{
					
				if($post_categories[0]->name == "Company"){
					$class = "blogroll-company";
					
				}elseif($post_categories[0]->name == "Reisen"){
					$class = "blogroll-reisen";
					$link_name = "Cooming Soon";
				} else {
					$link_name = "Cooming Soon";
					$class = "blogroll-reisen";
				}
					
				
				if($post_categories[0]->name == "Company"){
					$link_name = "Jetzt anmelden";
				} else {
					$link_name = "Mehr lesen";
				 }
				
				
				if($soon === true){
					$content = "<div class='".$class."' style='background: url(".$feature_img.") no-repeat center bottom;'><a href='".$single_url."'><div class='overlay'></div></a>";
					$content .= '<div class="full"><div><h3><span>'.$single_title.'</span></h3><span>'.$date.'</span>';
					
					if($schiff === true){
						$content .= '<p>Region: '.$region. '<br/> Boot: '.$boot.'<br/> Mannschaft: '.$mannschaft.'</p>';
					} else{			
						$content .= '<p>Ort: '.$ort.'</p>';	
				}
					$content .= '<p>'.$auszug.'</p>';
				$content .= '<div class="caption"><p>'.$beschreibung.'</p></div><div id="buttons-18-4-0-2" class="module module-buttons buttons-18-4-0-2 buttom_red left " data-id="b2486b4">           
        <div class="module-buttons normal solid squared"><div class="module-buttons-item  buttons-horizontal"><a class="ui builder_button"><span>Coming Soon</span></a></div></div></div>
    </div></div></div>';
					
				}	else {
					$content = "<div class='".$class."' style='background: url(".$feature_img.") no-repeat center bottom;'><a href='".$single_url."'><div class='overlay'></div></a>";	
				$content .= '<div class="full"><div><h3><span>'.$single_title.'</span></h3><span>'.$date.'</span>';
				$content .= '<p>Ort: '.$ort.'</p>';
				$content .= '<p>'.$auszug.'</p>';
				$content .= '<div class="caption"><p>'.$beschreibung.'</p></div>
				<div id="buttons-18-4-0-2" class="module module-buttons buttons-18-4-0-2 buttom_red left " data-id="b2486b4">           
        <div class="module-buttons normal solid squared"><div class="module-buttons-item  buttons-horizontal"><a href="'.$single_url.'" class="ui builder_button"><span>'.$link_name.'</span></a></div></div></div>
    </div></div></div>';
				}
					
					
				
				}
		
	  }	
	 
	 // otherwise returns the database content
  return $content;
}

add_filter( 'the_excerpt', 'my_the_content_filter', 20 );


/* Woocommerce - Display Product Category Title on Shop Page */
add_action( 'woocommerce_before_shop_loop', 'shop_product_cat', 8);
function shop_product_cat(){
    global $product;
    
    $cat_args = array(
        'orderby'    => 'name',
        'order'      => 'asc',
        'hide_empty' => true,
    );

    $product_cats = get_terms( 'product_cat', $cat_args );
    
    echo '<h3 class="woocommerce-cate-title">FILTER</h3>';
    echo "<ul class='woocommerce-category_title'>";
    echo '<li class="category_title all active" data-attr="all">all</li>';
    
    foreach($product_cats as $key => $cat){
        echo '<li class="category_title '.$cat->slug.'" data-attr="'.$cat->slug.'">'.$cat->name.'</li>';
        
    }
    echo "</ul>";
}


/* Woocommerce - single product Page back to shop link */
add_action( 'woocommerce_single_product_summary', 'back_to_shop_link', 35);
function back_to_shop_link(){
    echo '<a href="'.home_url().'/shop" class="back-to-shop"><i class="fa fa-long-arrow-left"></i> ' . esc_html( 'ZURUCK ZUM SHOP' ) . '</a>' ;
}

/**
* shop page hook remove action
*/
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10);
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

add_action( 'woocommerce_shop_loop_item_title', 'woo_show_excerpt_shop_page', 15 );
function woo_show_excerpt_shop_page() {
	global $product;
    
    if(!empty($product->post->post_excerpt)){
        echo '<p>' .wp_trim_words( $product->post->post_excerpt , '10', ' ' ). '</p>'; 
    }
    
}

/**
* single product hook remove action
*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


/**
* single product description 
*/
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_breadcrumb', 10);

add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_single_description', 20);
function woocommerce_template_single_description(){
    echo '<div class="product-description">';
    the_content();
    echo '</div>';
}


add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_single_add_to_cart_bottom', 25);
function woocommerce_template_single_add_to_cart_bottom() {
    global $product;
    echo '<div class="bottom-add-to-cart" id="additional-add-to-cart">';
    do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' );
    echo '</div>';
}


/**
 * Change the breadcrumb separator
 */
function urban_tribe_breadcrumb_delimiter( $defaults ) {
	$defaults['delimiter'] = ' <span>&gt;</span> ';
	return $defaults;
}
add_filter( 'woocommerce_breadcrumb_defaults', 'urban_tribe_breadcrumb_delimiter', 20 );

/**
 * Add Cart icon to menu
 */
add_action( 'genesis_header', 'jmd_wc_cart_count' );
function jmd_wc_cart_count() {
 
    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
        $count = WC()->cart->cart_contents_count;
        ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php if ( $count > 0 ) echo '(' . $count . ')'; ?></a><?php
    
    }
 
}
//* Ensure cart contents update when products are added to the cart via AJAX
add_filter( 'woocommerce_add_to_cart_fragments', 'jmd_header_add_to_cart_fragment' );
function jmd_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php if ( $count > 0 ) echo '(' . $count . ')'; ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
		
}












