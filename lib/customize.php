<?php
/**
 * Genesis Sample.
 *
 * This file adds the Customizer additions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0-or-later
 * @link    https://www.studiopress.com/
 */

add_action( 'customize_register', 'genesis_sample_customizer_register' );
/**
 * Registers settings and controls with the Customizer.
 *
 * @since 2.2.3
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function genesis_sample_customizer_register( $wp_customize ) {

	$wp_customize->add_setting(
		'genesis_sample_link_color',
		array(
			'default'           => genesis_sample_customizer_get_default_link_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'genesis_sample_link_color',
			array(
				'description' => __( 'Change the color of post info links, hover color of linked titles, hover color of menu items, and more.', 'genesis-sample' ),
				'label'       => __( 'Link Color', 'genesis-sample' ),
				'section'     => 'colors',
				'settings'    => 'genesis_sample_link_color',
			)
		)
	);

	$wp_customize->add_setting(
		'genesis_sample_accent_color',
		array(
			'default'           => genesis_sample_customizer_get_default_accent_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'genesis_sample_accent_color',
			array(
				'description' => __( 'Change the default hovers color for button.', 'genesis-sample' ),
				'label'       => __( 'Accent Color', 'genesis-sample' ),
				'section'     => 'colors',
				'settings'    => 'genesis_sample_accent_color',
			)
		)
	);

	$wp_customize->add_setting(
		'genesis_sample_logo_width',
		array(
			'default'           => 350,
			'sanitize_callback' => 'absint',
		)
	);

	// Add a control for the logo size.
	$wp_customize->add_control(
		'genesis_sample_logo_width',
		array(
			'label'       => __( 'Logo Width', 'genesis-sample' ),
			'description' => __( 'The maximum width of the logo in pixels.', 'genesis-sample' ),
			'priority'    => 9,
			'section'     => 'title_tagline',
			'settings'    => 'genesis_sample_logo_width',
			'type'        => 'number',
			'input_attrs' => array(
				'min' => 100,
			),

		)
	);
    
    
    
    // Shop and Product page custom shortcode support
    $wp_customize->add_section('urban_tribe_shop_shortcode', array(
        'title' => __('Urban Tribe Shop', 'genesis-sample'),
        'priority' => 10,
    ));
    
    //--hero slider shortcode--
    $wp_customize->add_setting('urban_tribe_shop_hero_slider', array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_input_text',
    ));
    $wp_customize->add_control('urban_tribe_shop_hero_slider_ctrl',
        array(
            'label' => __('Hero Slider Shortcode', 'genesis-sample'),
            'section' => 'urban_tribe_shop_shortcode',
            'settings' => 'urban_tribe_shop_hero_slider',
            'type' => 'text'
        )
    );
    
    //--Reference shortcode--
    $wp_customize->add_setting('urban_tribe_shop_reference_shortcode', array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_input_text',
    ));
    $wp_customize->add_control('urban_tribe_shop_reference_shortcode_ctrl',
        array(
            'label' => __('Reference Shortcode', 'genesis-sample'),
            'section' => 'urban_tribe_shop_shortcode',
            'settings' => 'urban_tribe_shop_reference_shortcode',
            'type' => 'text'
        )
    );
    
    //--Map shortcode--
    $wp_customize->add_setting('urban_tribe_shop_map_shortcode', array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_input_text',
    ));
    $wp_customize->add_control('urban_tribe_shop_map_shortcode_ctrl',
        array(
            'label' => __('Map Shortcode', 'genesis-sample'),
            'section' => 'urban_tribe_shop_shortcode',
            'settings' => 'urban_tribe_shop_map_shortcode',
            'type' => 'text'
        )
    );
    
    
    // Shop and Product page custom shortcode support
    $wp_customize->add_section('urban_tribe_product_shortcode', array(
        'title' => __('Urban Tribe Product', 'genesis-sample'),
        'priority' => 15,
    ));
    
    
    //--Reference shortcode--
    $wp_customize->add_setting('urban_tribe_product_reference_shortcode', array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_input_text',
    ));
    $wp_customize->add_control('urban_tribe_product_reference_shortcode_ctrl',
        array(
            'label' => __('Reference Shortcode', 'genesis-sample'),
            'section' => 'urban_tribe_product_shortcode',
            'settings' => 'urban_tribe_product_reference_shortcode',
            'type' => 'text'
        )
    );
    
    //--Map shortcode--
    $wp_customize->add_setting('urban_tribe_product_map_shortcode', array(
        'default' => '',
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_input_text',
    ));
    $wp_customize->add_control('urban_tribe_product_map_shortcode_ctrl',
        array(
            'label' => __('Map Shortcode', 'genesis-sample'),
            'section' => 'urban_tribe_product_shortcode',
            'settings' => 'urban_tribe_product_map_shortcode',
            'type' => 'text'
        )
    );
    
    function sanitize_input_text($data){
        return sanitize_text_field($data);
    }

}
